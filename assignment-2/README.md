# Assignment 2

Write pipeline-as-code to deploy containerised application. You can choose any sample application written in java, nodejs or php. 

- Prepare a dockerfile or docker-conpose.yaml to run the containerised application.

- Use any of the container orchestration tool, be it kubernetes, docker swarm, ecs, eks etc and deploy the above application to that cluster.

- This deployment pipeline should be managed in a code format, you use can jenkinsfile, drone-ci, azure pipelines or bitbucket-pipelines etc as the tool for achieving the same. 

Delivery Outcome:
- Once we make changes in the docker-compose.yaml or dockerfile or any of the code in the container and push it to git, your pipeline as a code should build this container and deploy it accordingly to the mentioned services i.e kubernetes, docker-swarm, ecs, eks etc.

- To test we should have an url endpoint which confirms that the application is running.

# Solution

## Sample node js application

- This directory contains 3 files related to the node js app.
  - `package.json` and `package-lock.json` defines the npm package dependencies
  - server.js is the sample code which prints `hello world` and runs on port `8080`

- This directory also has `Dockerfile` which is responsible for building and tagging the sample nodejs image.
- At the root level of this repository, there is a `bitbucket-pipelines.yml` file which has two steps:
  - Step 1:
    - builds and tags docker images of our sample nodejs app using bitbucket's inbuilt `docker` service
    - pushes docker image to AWS ECR using [`aws-ecr-push-image`](https://bitbucket.org/atlassian/aws-ecr-push-image/src/master/) pipe to push an image to AWS Elastic Container Registry
  - Step 2:
    - Use [governmentpaas/awscli](https://hub.docker.com/r/governmentpaas/awscli) docker image for running aws cli commands  
    - Create new task definition and register its new version in the AWS ECS
    - Then it updates the ECS Service with new task definition
    - The ECS Service update triggers a rolling update on the cluster and deploys the new docker image

## AWS Configuration

- Make sure you have `ecsTaskExecutionRole` role created in your aws account: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_execution_IAM_role.html#create-task-execution-role
- Edit the `task-def/task-definition.json` file in this repository and add the `ecsTaskExecutionRole` ARN value at `executionRoleArn` key. 
- Create AWS ECR repository

```
aws ecr create-repository --repository-name nodejs-sample-app-repo --region us-east-1
```

- Create AWS ECS cluster of type `Fargate` in default region us-east-1

```
aws ecs create-cluster --cluster-name nodejs-app-cluster --capacity-providers FARGATE --region us-east-1
```

- Create a task definition for the cluster. The initial template for task definiton is given under `task-def` folder

```
aws ecs register-task-definition --cli-input-json file://task-def/task-definition.json
```

- Now we need to create a ECS Service to run the tasks using above task definition. But before that, we need to configure ALB, target group and security groups which we will be using with service definition

  1. Create a Target group of type `IP` and use port 8080 since our docker container exposes that port in task definition

```
DEFAULT_VPC_ID=$(aws ec2 describe-vpcs --filters Name=is-default,Values=true --query "Vpcs[].VpcId" --output text)

aws elbv2 create-target-group \
    --name ecs-target-group \
    --protocol HTTP \
    --port 8080 \
    --target-type ip \
    --vpc-id $DEFAULT_VPC_ID
```

  2. Create a security groups for public ALB and ECS service

```
SUBNET_A=$(aws ec2 describe-subnets --filters "Name=vpc-id,Values=$DEFAULT_VPC_ID" "Name=availability-zone,Values=us-east-1a" "Name=default-for-az,Values=true" --query "Subnets[].SubnetId" --output text)

SUBNET_B=$(aws ec2 describe-subnets --filters "Name=vpc-id,Values=$DEFAULT_VPC_ID" "Name=availability-zone,Values=us-east-1b" "Name=default-for-az,Values=true" --query "Subnets[].SubnetId" --output text)

PUBLIC_ALB_SG_ID=$(aws ec2 create-security-group --group-name public-alb-sg --vpc-id $DEFAULT_VPC_ID --description "Public ALB security group" | jq -r ".GroupId")

ECS_TASKS_SG_ID=$(aws ec2 create-security-group --group-name ecs-tasks-sg --vpc-id $DEFAULT_VPC_ID --description "Security group for ECS tasks" | jq -r ".GroupId")

aws ec2 authorize-security-group-ingress \
    --group-id $PUBLIC_ALB_SG_ID \
    --protocol tcp \
    --port 80 \
    --cidr 0.0.0.0/0

aws ec2 authorize-security-group-ingress \
    --group-id $ECS_TASKS_SG_ID \
    --protocol tcp \
    --port 8080 \
    --source-group $PUBLIC_ALB_SG_ID
```

  3. Creat public Application load balancer

```  
PUBLIC_ALB_ARN=$(aws elbv2 create-load-balancer --name public-alb --type application --scheme internet-facing --subnets $SUBNET_A $SUBNET_B --security-groups $PUBLIC_ALB_SG_ID | jq -r ".LoadBalancers[].LoadBalancerArn")
```

  4. Attach the `ecs-target-group` to the `public-alb`

```  
ECS_TARGETS_ARN=$(aws elbv2 describe-target-groups --name ecs-target-group | jq -r ".TargetGroups[].TargetGroupArn")

aws elbv2 create-listener \
    --load-balancer-arn $PUBLIC_ALB_ARN \
    --protocol HTTP \
    --port 80 \
    --default-actions Type=forward,TargetGroupArn=$ECS_TARGETS_ARN
```

  5. Create the ECS service with given ALB and security groups. Somehow aws cli wasn't working properly for setting up ECS service for attaching ALB with Fargate. 
  
  - However, you can create the service using AWS console.

    - Go to ECS cluster - `nodejs-app-cluster`
  
    - Under Service tab -> Create service
    
    - Choose Launch type as Fargate

    - Task definition: nodejs-task

    - Revision: Latest

    - Cluster: nodejs-app-cluster

    - Service name: node-service

    - Number of tasks: 2

    - Leave others as default -> click next

    - While configuring network, choose default vpc and default subnets in us-east-1a and us-east-1b

    - Choose the already created security group  above `ecs-tasks-sg`

    - Choose Load balancer as Application Load balancer

    - Choose the already created target group above `ecs-target-group`

    - Leave other as default -> click next

    - Create service  
 

## Bitbucket configuration

- Enable pipelines in your bitbucket repo 
- Go to `Repository settings` --> `Repository variables` and add the following variables. Please provide values according to your aws account.

| Name      | Value | 
| ----------- | ----------- | 
| AWS_SECRET_ACCESS_KEY      | XXXXX | 
| AWS_ACCESS_KEY_ID   | XXXXX        |
| AWS_ECR_REPO_URL   | <aws_account_number>.dkr.ecr.us-east-1.amazonaws.com/nodejs-sample-app-repo | 

- Any subsequent changes in the code will trigger this pipeline. It will build docker image, push it to ECR, create new task definition, update the ECS service and start a rolling deployment. 

- Once the changes are deployed, hit the public load balancer url. You will see the updated code as soon as ecs tasks become healthy under load balancer.
