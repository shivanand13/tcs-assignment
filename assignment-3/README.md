# Assignment 3

Create a script using any preferred programming language (python, Node.js, bash etc.) to check if a given string is palindrome.
- Create the script which can take the string as an input and provide the necessary result

Delivery Outcome:

-Input: script.sh level

 Output: Provided string "level" is palindrome

-Input: script.sh bash

 Output: Provided string "bash" is not a palindrome

# Solution

Run this script `check_palindrome.py` as following:

- `python3 check_palindrome.py level` 

Output: Provided string "level" is palindrome

- `python3 check_palindrome.py bash` 

Output: Provided string "bash" is NOT a palindrome