#!/usr/bin/python3

import sys

# Get the argument from command line
given_string = str(sys.argv[1])

def checkPalindrome(str):
  # The string slicing str[::-1] reveress the string
  if(str == str[::-1]):
    print("Provided string \"" + str + "\" is palindrome")
  else:
    print("Provided string \"" + str + "\" is NOT a palindrome")

checkPalindrome(given_string)