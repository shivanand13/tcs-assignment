variable "aws_region" {
  description = "AWS region to create vpc and its components"
  type        = string
  default     = ""
}

variable "instance_type" {
  description = "Instance type for servers"
  type        = string
  default     = "t2.micro"
}

variable "rds_admin" {
  type        = string
  description = "username for rds admin user"
  default     = ""
  sensitive   = true
}

variable "rds_password" {
  type        = string
  description = "password for rds admin user"
  default     = ""
  sensitive   = true
}